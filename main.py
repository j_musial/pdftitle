# get_doc_info.py

from PyPDF2 import PdfFileReader
import os

def usuwanie(nazwa):
    nazwa2=''
    for x in nazwa:
        if x != '|' and x != '/' and x != '\\' and x != '?' and x != '%' and x !='*' and x != ':' and x != '"' and x != '<' and x != '>':
            nazwa2=nazwa2+x
    return(nazwa2)

def get_info(path):
    with open(path, 'rb') as f:
        pdf = PdfFileReader(f)
        info = pdf.getDocumentInfo()
        number_of_pages = pdf.getNumPages()

    # print(info)
    #print(pdf)
    author = info.author
    creator = info.creator
    producer = info.producer
    subject = info.subject
    title = info.title
    print(info)
    return title, author
    # print(author)


if __name__ == '__main__':
    print("Podaj ścieżke do folderu z plikami pdf:")
    sciezka = input()
    print(sciezka)
    list = os.listdir(sciezka)
    print(list)
    numer=1
    for plik in list:
        path: str = sciezka + "\\" + plik
        #path = os.path(sciezka,plik)
        tyt, aut = get_info(path)
        print(tyt,aut)
        if tyt != None  and aut != None:
            nowa_nazwa=tyt + ', ' + aut + '.pdf'
            if nowa_nazwa == ', .pdf':
                nowa_nazwa='Artykuł' + str(numer)
                numer=numer+1
            #nowa_nazwa = sciezka + '\\' + nowa_nazwa
            nowa_nazwa=usuwanie(nowa_nazwa)
            os.rename(path, nowa_nazwa)
        elif tyt != None and aut == None:
            nowa_nazwa = tyt + '.pdf'
            if nowa_nazwa == '.pdf':
                nowa_nazwa='Artykuł' + str(numer)
                numer=numer+1
           # nowa_nazwa = sciezka + '\\' + nowa_nazwa
            nowa_nazwa = usuwanie(nowa_nazwa)
            os.rename(path, nowa_nazwa)

